Ext.define('Latihan2.view.tree.TreeListModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.tree-list',

    formulas: {
        selectionText: function(get) {
            var selection = get('treelist.selection'),
                path;
            if (selection) {
                path = selection.getPath('text');
                path = path.replace(/^\/Root/, '');
                return 'Selected: ' + path;
            } else {
                return 'No node selected';
            }
        }
    }, 

    stores: {
        navItems: {
            type: 'tree',
            rootVisible: false,
            root: {
                expanded: true,
                text: 'All',
                iconCls: 'x-fa fa-sitemap',
                children: [{
                    text: 'Teknik',
                    iconCls: 'x-fa fa-home',
                    children: [{
                        text: 'Teknik Informatika',
                        iconCls: 'x-fa fa-user',
                        leaf: true
                    },{
                        text: 'Teknik Sipil',
                        iconCls: 'x-fa fa-user',
                        leaf: true
                    },{
                        text: 'Teknik Perminyakan',
                        iconCls: 'x-fa fa-user',
                        leaf: true
                    }]
                }]
            }
        }
    }
});