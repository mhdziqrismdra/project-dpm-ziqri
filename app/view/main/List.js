Ext.define('Latihan2.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'Latihan2.store.Personnel',
        'Ext.grid.plugin.Editable'
    ],

    plugins: [{
        type: 'grideditable'
    }],

    title: 'Data Mahasiswa Sencha',

    /*store: {
        type: 'personnel'
    },*/

    bind: '{personnel}',

    viewModel: {
        stores:{
            personnel:{
                type: 'personnel'
            }
        }
    },

    columns: [
        { text: 'Npm',  dataIndex: 'npm', width: 120 },
        { text: 'Name',  dataIndex: 'nama', width: 230, editable: true },
        { text: 'Email', dataIndex: 'email', width: 230, editable: true },
        { text: 'Phone', dataIndex: 'phone', width: 150, editable: true }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
