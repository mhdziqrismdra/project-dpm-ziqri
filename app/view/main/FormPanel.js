Ext.define('Latihan2.view.main.Form-Panel', {
    extend: 'Ext.form.Panel',
    xtype: 'editform',
    id: 'editform',

    shadow: true,
    items: [
        {
            xtype: 'textfield',
            name: 'name',
            id: 'myname',
            label: 'Name',
            placeHolder: 'Your Name',
            autoCapitalize: true,
            required: true,
            clearIcon: true
        },
        {
            xtype: 'emailfield',
            name: 'email',
            id: 'myemail',
            label: 'Email',
            placeHolder: 'me@sencha.com',
            clearIcon: true
        },
        {
            xtype: 'textfield',
            name: 'phone',
            id: 'myphone',
            label: 'Phone',
            placeHolder: '0813-2343-2345',
            autoCapitalize: true,
            required: true,
            clearIcon: true
        },
        {
            xtype: 'button',
            ui: 'action',
            text: 'Simpan Perubahan',
            handler: 'onSimpanPerubahan'
        },{
            xtype: 'button',
            ui: 'confirm',
            text: 'Tambah Personnel',
            handler: 'onTambahPersonnel'
        }         
    ]
});