/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('Latihan2.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',
    id: 'app-main',

    requires: [
        'Ext.MessageBox',

        'Latihan2.view.main.MainController',
        'Latihan2.view.main.MainModel',
        'Latihan2.view.main.List',
        'Latihan2.view.user.User-Form',
        'Latihan2.view.group.Carousel-Page',
        'Latihan2.view.setting.BasicDataView',
        'Latihan2.view.user.Login-Form',
        'Latihan2.view.chart.Column',
        'Latihan2.view.chart.Radar',
        'Latihan2.view.chart.Area',
        'Latihan2.view.tree.TreePanel',
        'Latihan2.view.setting.PanelDataView',
        'Latihan2.view.main.Form-Panel',
        'Latihan2.view.chart.Bar',
        'Latihan2.store.Bar',
        'Latihan2.view.chart.ListBar'
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        {
            xtype: 'toolbar',
            docked: 'top',
            items: [{
                xtype: 'button',
                text: 'Read',
                ui: 'action',
                scope: this,
                listeners: {
                    tap: 'onReadClicked'
                }
            }]
        },
        {
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'panel',
                layout: 'hbox',
                items: [{
                    xtype: 'mainlist',
                    flex: 2
                },{
                    xtype: 'paneldataview',
                    flex: 1
                },{
                    xtype: 'editform',
                    flex: 1
                }]
            }]
        },{
            title: 'Users',
            iconCls: 'x-fa fa-user',
            layout: 'fit',
            items: [{
                xtype: 'user-form'
            }]
        },{
            title: 'Chart',
            iconCls: 'x-fa fa-users',
            layout: 'fit',
            items: [{
                xtype: 'panel',
                layout: 'vbox',
                items: [{
                    xtype: 'bar',
                    flex: 1
                },{
                    xtype: 'listbar',
                    flex: 1
                }]
            }]
        },{
            title: 'Settings',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            items: [{
                
            }]
        },{
            title: 'Chart',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            items: [{
                xtype: 'area-chart'
            }]
        },{
            title: 'Tree',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            items: [{
                xtype: 'tree-panel'
            }]
        }
    ]
});
