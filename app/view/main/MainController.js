/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('Latihan2.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onItemSelected: function (sender, record) {
        /*var nama = record.data.name;
        var npm = record.data.npm;
        localStorage.setItem('nama', nama);
        localStorage.setItem('npm', npm);
        Ext.Msg.confirm('Konfirmasi', 'Yakin lu '+nama+ '?', 'onConfirm', this);
        console.log(record.data);*/

        //alert(record.data.nama);
        Ext.getStore('detailpersonnel').getProxy().setExtraParams({
            npm: record.data.npm
        });
        Ext.getStore('detailpersonnel').load();
    },

    onBarItemSelected: function (sender, record) {
        
    },

    onConfirm: function (choice) {
        var nama = localStorage.getItem('nama', nama);
        var npm = localStorage.getItem('npm', npm);
        if (choice === 'yes') {
            alert('Makasih ya!, '+nama+' ('+npm+')');
        }
        else{
            alert('Yaudah deh');
        }
    },

    onClickButton: function (){
        Ext.Msg.confirm('Logout', 'Apakah mau logout?', 'onKonfirmasi', this);
    },

    onKonfirmasi: function(choice){
        if (choice == 'yes'){
            localStorage.removeItem("logeddin");
            this.getView().destroy();
            Ext.getCmp('login').destroy();
            this.overlay = Ext.Viewport.add({
                xtype: 'login',
                floated: true,
                //modal: true,
                //hideOnMaskTap: true,
                showAnimation: {
                    type: 'popIn',
                    duration: 250,
                    easing: 'ease-out'
                },
                hideAnimation: {
                    type: 'popOut',
                    duration: 250,
                    easing: 'ease-out'
                },
               // centered: true,
                width: "100%",
                height: "100%",
                scrollable: true
            });
            this.overlay.show();
        }
    },

    onReadClicked: function(){
        Ext.getStore('personnel').load();
    },

    onSimpanPerubahan: function(){
        nama = Ext.getCmp('myname').getValue();
        email = Ext.getCmp('myemail').getValue();
        phone = Ext.getCmp('myphone').getValue();

        store = Ext.getStore('personnel');
        record = Ext.getCmp('mydataview').getSelection();
        index = store.indexOf(record);
        record = store.getAt(index);
        store.beginUpdate();
        record.set('nama', nama);
        record.set('email', email);
        record.set('phone', phone);
        store.endUpdate();
        alert("Updating..");
    },
    onTambahPersonnel: function(){
        nama = Ext.getCmp('myname').getValue();
        email = Ext.getCmp('myemail').getValue();
        phone = Ext.getCmp('myphone').getValue();

        store = Ext.getStore('personnel');
        store.beginUpdate();
        store.insert(0, {'nama':nama, 'email':email, 'phone':phone})
        store.endUpdate();
        alert("Inserting..");
    }
});

function onDeletePersonnel (npm){
    record = Ext.getCmp('mydataview').getSelection();
    Ext.getStore('personnel').remove(record);
    alert(npm);
}

function onEditPersonnel (npm){
    record = Ext.getCmp('mydataview').getSelection();
    nama = record.data.nama;
    email = record.data.email;
    phone = record.data.phone;
    Ext.getCmp('myname').setValue(nama);
    Ext.getCmp('myemail').setValue(email);
    Ext.getCmp('myphone').setValue(phone);
}
