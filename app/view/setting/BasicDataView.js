Ext.define('Latihan2.view.setting.BasicDataView', {
    extend: 'Ext.Container',
    xtype: 'basicdataview',
    id: 'status',
    requires: [
    'Ext.dataview.plugin.ItemTip',
    'Ext.plugin.Responsive',
    'Latihan2.store.Personnel',
    'Ext.field.Search',
    ],

    /*viewModel: {
        stores:{
            personnel:{
                type: 'personnel'
            }
        }
    },*/

    
    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    items: [/*{
        docked: 'top',
        xtype: 'toolbar',
        items: [
            {
                xtype: 'searchfield',
                placeHolder: 'Filter by Nama',
                name: 'searchfield',
                listeners: {
                    change: function (me, newValue, oldValue, eOpts){
                        personnelStore = Ext.getStore('personnel');
                        personnelStore.filter('name', newValue);
                    }
                }
            },
            {
                xtype: 'spacer'
            },
            {
                xtype: 'searchfield',
                placeHolder: 'Filter by NPM',
                name: 'searchfield',
                listeners: {
                    change: function (me, newValue, oldValue, eOpts){
                        personnelStore = Ext.getStore('personnel');
                        personnelStore.filter('npm', newValue);
                    }
                }
            },
        ]
    },{
        docked: 'bottom',
        xtype: 'toolbar',
        items: [
            {
                xtype: 'searchfield',
                placeHolder: 'Filter by No. HP',
                name: 'searchfield',
                listeners: {
                    change: function (me, newValue, oldValue, eOpts){
                        personnelStore = Ext.getStore('personnel');
                        personnelStore.filter('phone', newValue);
                    }
                }
            }
        ]
    },*/{
        xtype: 'dataview',
        scrollable: 'y',
        cls: 'dataview-basic',
        itemTpl: /*'<table style="border-spacing:3px;border-collapse:separate">' + 
            //'<tr><td rowspan = 6><img class="img" src="resources/images/{photo}" width=100 ></td></tr>' +
            '<tr><td><font size = "4"><b>{nama}</b></font></td></tr>' +
            '<tr><td><font color = "grey"><i>{npm}</i></font></td></tr>' + 
            '<tr><td>{jurusan}</td></tr>' +
            '<tr><td>Email: {email}</td></tr>' + 
            '<tr><td>Phone: {phone}</td></tr>' +  
            '<hr>',*/
           // '<div class="img" style="background-image: url({photo});"></div>'+
            '<div class="content">'+
            '<div class="npm"><font color = "grey"><i>{npm}</i></font></div>'+
            '<div class="nama"><font size = "4"><b>{nama}</b></font></div>'+
            '<div class="jurusan">{jurusan}</div>'+
            '<div class="email">{email}</div>'+
            '<div class="phone">{phone}</div>'+
            '</div><hr>',
        bind: {
            store: '{personnel}'
        },
        plugins: {
            type: 'dataviewtip',
            align: 'l-r?',
            plugins: 'responsive',
            
            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 600,
            minWidth: 300,
            delegate: '.img',
            allowOver: true,
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' + 
            '<tr><td>NPM: </td><td>{npm}</td></tr>' +
            '<tr><td>Nama: </td><td>{name}</td></tr>' + 
            '<tr><td>Email: </td><td>{email}</td></tr>' +
            '<tr><td>Phone: </td><td>{phone}</td></tr>'
        }
    }]
});