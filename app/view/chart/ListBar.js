Ext.define('Latihan2.view.chart.ListBar', {
    extend: 'Ext.grid.Grid',
    xtype: 'listbar',

    title: 'List Bar',

    requires: [ 
        'Ext.grid.plugin.Editable',
    ],

    plugins: [{
        type: 'grideditable'
    }],

    bind: {
        store: '{bar}'
    },

    viewModel: {
        stores:{
            bar:{
                type: 'bar'
            }
        }
    },

    columns: [
        { text: 'Name',  dataIndex: 'name', width: 250, editable: true},
        { text: 'g1', dataIndex: 'g1', width: 250, editable: true},
        { text: 'g2', dataIndex: 'g2', width: 150, editable: true },
        { text: 'g3', dataIndex: 'g3', width: 150, editable: true },
        { text: 'g4', dataIndex: 'g4', width: 150, editable: true },
        { text: 'g5', dataIndex: 'g5', width: 150, editable: true },
        { text: 'g6', dataIndex: 'g6', width: 150, editable: true }
    ],

    listeners: {
        select: 'onBarItemSelected'
    }
});