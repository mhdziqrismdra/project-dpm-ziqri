/**
 * Demonstrates how to use an Ext.Carousel in vertical and horizontal configurations
 */
Ext.define('Latihan2.view.group.Carousel-Page', {
    extend: 'Ext.Container',
    xtype: 'my-carousel',

    requires: [
        'Ext.carousel.Carousel'
    ],

    cls: 'cards',
    shadow: true,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },


    /*defaults: {
        cls: 'demo-solid-background',
        flex: 1
    },*/
    items: [{
        xtype: 'carousel',
        flex: 1,
        items: [{
            html: '<p>Swipe left to show the next card…</p>',
        },
        {
            html: '<p>You can also tap on either side of the indicators.</p>',
        },
        {
            html: 'Card #3',
        }]
    }, 
    {
        xtype: 'spacer'
    },
    {
        xtype: 'carousel',
        flex: 2,
        ui: 'light',
        items: [{
            html: '<p>Carousels can also be vertical <em>(swipe up)…</p>',
        },
        {
            html: 'And can also use <code>ui:light</code>.',
        },
        {
            html: 'Card #3',
        }]
    }]
});