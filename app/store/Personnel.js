Ext.define('Latihan2.store.Personnel', {
    extend: 'Ext.data.Store',
    alias: 'store.personnel',
    storeId: 'personnel',
    autoLoad: true,
    autoSync: true,
    fields: [
        'npm',   'nama', 'email', 'phone', 'jurusan'
    ],

    /*data: { items: [
        { photo: 'gambar1.jpg', npm: '183510119', name: 'Muhammad Ziqri S', email: "ziqri@students.uir.ac.id", phone: "0852-1324-2343", jurusan: "Teknik Informatika" },
        { photo: 'gambar2.png', npm: '183510117', name: 'Marko Florentivo', email: "marko@students.uir.ac.id", phone: "0822-5424-6342", jurusan: "Teknik Informatika" },
        { photo: 'gambar3.jpg', npm: '183510122', name: 'Jamaludin', email: "jamal@students.uir.ac.id", phone: "0812-1364-1765", jurusan: "Teknik Perminyakan" },
        { photo: 'gambar4.jpeg', npm: '183510213', name: 'Vino Aiskap', email: "vino@students.uir.ac.id", phone: "0853-6324-5383", jurusan: "Teknik Mesin" },
        { photo: 'gambar5.jpg', npm: '183510219', name: 'Bella Sukamti', email: "bella@students.uir.ac.id", phone: "0822-9624-2644", jurusan: "Teknik Sipil" }
    ]},*/


    proxy: {
        api: {
            read: "http://localhost/MyApp/readDetailPersonnel.php",
            update: "http://localhost/MyApp/updatePersonnel.php",
            destroy: "http://localhost/MyApp/destroyPersonnel.php",
            create: "http://localhost/MyApp/createPersonnel.php"
            },
        type: 'jsonp',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    },

    listeners: {
        beforeload: function(store, operation, eOpts){
            this.getProxy().setExtraParams({
                npm : -1
            });
        }
    }
});
